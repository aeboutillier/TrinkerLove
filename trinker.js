const { reverse } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let male = [];
    for (x of p) {
      if (x.gender === "Male") male.push(x);
    }

    return male;
  },

  allFemale: function (p) {
    let female = [];
    for (x of p) {
      if (x.gender === "Female") female.push(x);
    }

    return female;
  },

  nbOfMale: function (p) {
    let man = 0;
    for (let x of p) {
      if (x.gender === "Male") man++;
    }
    return man;
  },

  nbOfFemale: function (p) {
    // let woman = p.length - this.nbOfMale(p);
    // return woman;
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function (p) {
    let mInter = 0;
    for (let x of p) {
      if (x.looking_for === "M") mInter++;
    }
    return mInter;
  },

  nbOfFemaleInterest: function (p) {
    let wInter = p.length - this.nbOfMaleInterest(p);
    return wInter;
  },

  match: function (p) {
    return "not implemented".red;
  },

  // Nombre de personnes qui gagnent plus de 2000$
  salaire_Plus2k: function (p) {
    let i = 0;
    for (let x of p) {
      if (salaireConvertion(x) > 2000) i++;
    }
    return i;
  },
  // Nombre de personnes qui aiment les Drama
  prefDrama: function (p) {
    let i = 0;
    for (let x of p) {
      let pref = x.pref_movie;
      if (pref.includes("Drama")) i++;
    }
    return i;
  },
  // Nombre de femmes qui aiment la science-fiction
  femme_PrefSF: function (p) {
    let i = 0;
    for (let x of this.allFemale(p)) {
      let pref = x.pref_movie;
      if (pref.includes("Sci-Fi")) i++;
    }
    return i;
  },

  // Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$
  documentaire_Thune: function (p) {
    let i = 0;
    for (let x of p) {
      let pref = x.pref_movie;
      if (pref.includes("Documentary") && salaireConvertion(x) > 1482) i++;
    }
    return i;
  },

  // Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$
  liste_Plus4k: function (p) {
    let list = [];
    for (let x of p) {
      if (salaireConvertion(x) > 4000) {
        list.push([x.id, x.first_name, x.last_name, x.income]);
      }
    }
    return list;
  },

  // Homme le plus riche (nom et id)
  homme_Riche: function (p) {
    let i = 0;
    let save = [];
    for (let x of this.allMale(p)) {
      if (salaireConvertion(x) > i) {
        i = salaireConvertion(x);
        save = [x.id, x.last_name, x.income];
      }
    }
    return save;
  },

  // Salaire Moyen
  salaire_Moyen: function (p) {
    let i = 0;
    for (let x of p) {
      i = salaireConvertion(x) + i;
    }
    return i / p.length;
  },

  // Salaire Median
  salaire_Median: function (p) {
    let tab = [];
    for (let x of p) {
      tab.push(salaireConvertion(x));
    }
    tab = tab.sort((a, b) => a - b);
    return tab[tab.length / 2];
  },

  // Nombre de personnes qui habitent dans l'hémisphère nord
  hemisphere_Nord: function (p) {
    let i = 0;
    for (let x of p) {
      if (x.latitude > 0) i++;
    }
    return i;
  },

  // Salaire moyen des personnes qui habitent dans l'hémisphère sud
  salaire_Moyen_Hemisphere_Sud: function (p) {
    let i = 0;
    let n = 0;
    for (let x of p) {
      if (x.latitude < 0) {
        n++;
        i = salaireConvertion(x) + i;
      }
    }
    return i / n;
  },

  // Personne qui habite le plus près de Bérénice Cawt (nom et id)
  plus_Proche: function (p) {},

  // Personne qui habite le plus près de Ruì Brach (nom et id)
  plus_Proche2: function (p) {},

  // les 10 personnes qui habite les plus près de Josée Boshard (nom et id)
  dix_Plus_Proche: function (p) {},

  // Les noms et ids des 23 personnes qui travaillent chez google
  google: function (p) {
    let i = [];
    for (let x of p) {
      if (x.email.includes("google")) i.push([x.id, x.first_name, x.last_name]);
    }
    return i;
  },

  //  Personne la plus agée
  plus_Agée: function (p) {
    let newtab = [];
    for (let x of p) {
      newtab.push({ age: calcAge(x), pers: x.first_name + " " + x.last_name });
    }
    newtab = newtab.sort((a, b) => a.age - b.age);

    return newtab[newtab.length - 1];
  },

  // Personne la plus jeune
  plus_Jeune: function (p) {
    let newtab = [];
    for (let x of p) {
      newtab.push({ age: calcAge(x), pers: x.first_name + " " + x.last_name });
    }
    newtab = newtab.sort((a, b) => a.age - b.age);

    return newtab[0];
  },

  // Moyenne des différences d'age
  moyenne_Différence_Age: function (p) {
    let ageTab = [];
    for (let x of p) {
      ageTab.push(calcAge(x));
    }

    let n = 0;
    let total = 0;

    for (let x of ageTab) {
      for (let y of ageTab) {
        total = total + Math.abs(x - y);
        n++;
      }
    }

    return total / (n - ageTab.length);
  },

  // __________________________________________________________________

  // __________________________________________________________________
};

function salaireConvertion(p) {
  let numberString = p.income.substring(1);
  let numberFloat = parseFloat(numberString);
  return numberFloat;
}

function calcAge(p) {
  let dob = p.date_of_birth;
  // const result = dob.split("-").join(", ");

  let age = new Date(dob);
  return ~~((Date.now() - age) / 31557600000);
  // Le nombre magique : 31557600000 est 24 * 3600 * 365.25 * 1000 Ce qui est la durée d'une année
}
